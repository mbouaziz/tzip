---
tzip: 021
title: Contract Multimedia Metadata
author: Josh Dechant (@codecrafting)
status: Work In Progress
type: Interface
created: 2020-11-12
---

## Abstract

This proposal is an extension of [TZIP-016][1] and describes a metadata standard for
multimedia asset(s) linked with contracts.

## Motivation

## Specification

## Rationale

## Backwards Compatibility

## Test Cases

## Implementations

## Appendix

## Copyright

Copyright and related rights waived via
[CC0][2].

[1]: https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-16/tzip-16.md
[2]: https://creativecommons.org/publicdomain/zero/1.0/
